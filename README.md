# PNNsMiceMachineVision

Data and code related to the work reported in the journal article:

> V. Javier Traver, Filiberto Pla, Marta Miquel, Maria Carbo-Gas, Isis Gil-Miravet, Julian Guarque-Chabrera  
> "Cocaine-induced preference conditioning: a machine vision perspective"  
> _Neuroinformatics_, 17:343-359, July, 2019 
> [DOI](https://doi.org/10.1007/s12021-018-9401-1)

In the description below, section numbers refer to sections in this article.

## Data

Data is in folder [**data**](https://gitlab.com/vtraver/PNNsMiceMachineVision/tree/master/data), and organized into subfolders, as follows:
* _PNNimages_: The PNN images used in this work, in PGM format. They were cropped from larger images as described in Section 2.3.
* _SIFTpoints_: The SIFT points obtained from the corresponding PNN images. They are required to build the HND descriptor (Section 2.4). Notice that only the location (x,y) of these points is used in this work, even though the full descriptor was written to file. 
* _HND_: The normalized HND descriptor (Section 2.4)

Where appropriate, subfolders are named as _sali_, _condi_ and _unpair_ to refer to their respective experimental groups: _Saline_, _Conditioned_ and _Unpaired_. 
Each file name along with its path identify its contents uniquely. For instance:  
 * `PNNimages/condi/img_mouse3_44.pgm` is the PNN image number 44 of mouse 3 in the conditioned group.  
 * `SIFTpoints/condi/img_mouse3_44.dat` contains the SIFT points for the example PNN image 
 * `HND/condi/img_mouse3_44.dat` contains the HND descriptor for the detected SIFT points in the example 

In the data folder, all non-image files are plain-text files.

## Code

Python code is in folder [**code**](https://gitlab.com/vtraver/PNNsMiceMachineVision/tree/master/code), with two files: **`config.py`** and **`main.py`**. Code is commented as a guide for the reader or programmer. In particular, reference to article parts are included for convenience. Parameters are by default set to the values reported in the paper.

### Tested environment

This code has been developed and tested under **Ubuntu 16.04.3 LTS** running on a **Intel(R) Core(TM) i7-6700HQ CPU @ 2.60GHz**.
However, it should work in many other environments both in Linux and Windows operating systems.

### Requirements

#### Python packages

You can install required packages from the provided `code/requirements.txt` file by running 

```bash
pip install -r requirements.txt
```

under folder **code**. If needed, further details can be found at the [user guide on pip](https://pip.pypa.io/en/stable/user_guide/#id1).

#### External and optional code

Although SIFT descriptors are already provided, if you wish to regenerate them (e.g. with different parameters or for different images) you need to install first the SIFT binary file available in VLFeat. More specifically:
1. Open a terminal and navigate to directory **sift**
2. Run the bash script **`install.sh`** from the command line

In case you want to test tSNE (Section 2.5.1), which is not actually required for the core part of the work, you first need to download and uncompress the author-provided Python code. Specifically:
1. Open a terminal and navigate to directory **tsne**
2. Run the bash script **`install.sh`** from the command line

**Note**:  
 The provided **`install.sh`** scripts use [*wget*](https://www.gnu.org/software/wget/) to download specific versions of the corresponding third-party software. If you do not have _wget_ installed, you should either install it first or download the archive by some other means. Similarly, if the source files were changed to another address, or a different version of the code was preferred, please update the URL accordingly. Once downloaded, the archive is uncompressed and extracted with [*zip*](https://en.wikipedia.org/wiki/Zip_(file_format)).

## Running the code

You may run the code either from your IDE of choice or from the command line:

```bash
python main.py
```

Some variables are given in a section of **`main.py`** to set paths of folders for input and/or output files, so that the user can easily change them (if wished).
Through the variable `partsToRun`, a subset of the parts given in `allParts` can be chosen to run only some parts of the work. The boolean _`bVerbose`_ let the user set whether some optional text is displayed on the console. 

### Optional or auxiliary output files

Graphical and numeric results or temporary information are saved to folder **outputs** for eventual consultation. For instance, if tSNE is tested, the results ([pickle](https://wiki.python.org/moin/UsingPickle) files and plot images) are written to the **outputs/tSNE** folder. Similarly, recognition performance (accuracies) with BoW and SVM are also written to (and loaded from) **outputs/accu** using 
pickle.

**Please, note** that since tSNE and _k_-means (for BoW) include stochastic components, their output may be similar, but not exactly the same, to that reported in the article. To visualize the outputs of the paper, please set `bUseTSNEfromPaper` (for tSNE) and `bUseAccFromPaper` (for BoW+SVM) to `True`.

### Utility programs 

Two utility programs are provided in **code/utils**, as follows.

#### Visualizing and saving PNN images with a color map 

Since the original PGM files have low contrast, it is hard to visualize them directly. The Python program **`saveImagesInColor.py`** is provided to display the image with the jet color map, and save the image in either the jet color map or with a scaled binary gray color map. Two output files are generated in **outputs/imgs**:
* **out.png**, the image in the chosen color map, and 
* **out_with_bar.png**, the image in the jet color map that includes the color scale bar. 

The input image path is given as a parameter. Optionally, a second parameter can be given to indicate whether the jet color map is desired. If the second parameter is not provided, the jet color map is assumed.

To facilitate running this Python program, please see (and run) the example given in the bash shell script **`saveImagesInColor.sh`**.

The output images can be displayed (e.g. with **`display`** program from command line). Since the output file names are always the same (regardless of the input file path and name), please rename or move them accordingly if multiple output files are required.

#### Visualizing and saving SIFT points on PNN images

It can be useful to observe the SIFT points detected on a PNN image. To that end, the Python program **`drawSIFTpoints.py`** is provided. To use it, just run it like any other python program, either from an IDE or from command line:

```bash
python drawSIFTpoints.py
```

Besides displaying the image on the screen, the resulting image is saved to file, whose path (**../../outputs**) and name are printed to inform the user.

#### Licenses and citation

Licenses for code and data are given, respectively, in files:
 * [`code/UNLICENSE`](https://gitlab.com/vtraver/PNNsMiceMachineVision/tree/master/code/UNLICENSE)
 * [`data/LICENSE`](https://gitlab.com/vtraver/PNNsMiceMachineVision/tree/master/data/LICENSE)

If you use any part of these code or data, citation of the following publication is appreciated: 


> V. Javier Traver, Filiberto Pla, Marta Miquel, Maria Carbo-Gas, Isis Gil-Miravet, Julian Guarque-Chabrera  
> "Cocaine-induced preference conditioning: a machine vision perspective"  
> _Neuroinformatics_, Vol(No):pages, Month, Year  
> [DOI](https://doi.org/10.1007/s12021-018-9401-1)

The corresponding BibTeX entry is:

```bibtex
@article{TraverNeuroinf,
   author = {Traver, V. Javier and Pla, Filiberto and Miquel, Marta and Carbo-Gas, Maria and  Gil-Miravet Isis and Guarque-Chabrera, Julian},
   title = {Cocaine-induced preference conditioning: a machine vision perspective},
   journal = {Neuroinformatics},
   year = 2019,
   month = jul,
   volume = 17,
   pages = {343--359},
}
```

