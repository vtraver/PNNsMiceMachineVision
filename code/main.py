#####################################################
# Work related to paper:
#
# V. Javier Traver, Filiberto Pla, Marta Miquel,
# Maria Carbo-Gas, Isis Gil-Miravet, Julian Guarque-Chabrera
# "Cocaine-induced preference conditioning:
#  a machine vision perspective"
#
# Version 1.0
#
# Corresponding author: V. Javier Traver
# vtraver@uji.es
# 2018
#####################################################

from PIL import Image
import numpy as np
import sys
import os
import glob
from os.path import basename, splitext
import config
from sklearn.neighbors import NearestNeighbors
import pickle
from sklearn.cluster import KMeans
import re
import pylab as plt

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from sklearn.model_selection import GridSearchCV
from sklearn import svm

from scipy import stats
from sklearn.metrics.pairwise import pairwise_distances

allParts = ['sift', 'hnd', 'tSNE', 'bow+svm', 'kde']

#####################################################
#  User preferences
#####################################################

# Some booleans
bVerbose = True  # display or not messages on console
bUseTSNEfromPaper = False  # if True, the same tSNE output as that reported in the paper is read and analysed; otherwise, the tSNE is run again and a new output produced
bUseAccFromPaper = False  # whether load the performances obtained for the paper or run BoW and SVM again
bAlsoUnpair = False # whether perform BoW+SVM also for the unpair group (Table 8)
bConfusionMatrix = True # whether to display the confusion matrix (Table 7)

# Relevant paths for main data
base_path = '../data/'  # where input/output folders and files are (or will be)
pnn_subfolder = 'PNNimages/'  # subfolder for PNN images
sift_subfolder = 'SIFTpoints/'  # subfolder for SIFT descriptors
hnd_subfolder = 'HND/'  # subfolder for HND descriptor

# Paths for the output of different algorithms
tsne_subfolder = '../outputs/tSNE/'  # subfolder for tSNE output
accu_subfolder = '../outputs/accu/'  # performance (accuracy) output of BoW+SVM
kde_subfolder = '../outputs/kde/' # selected PNNs according to clustering output in HND space

bAllParts = False  # whether to run all parts of the work
if bAllParts:
    partsToRun = allParts
else: # customize which parts of the work to run (the order of parts is hard-coded).
    partsToRun = ['bow+svm']  # Please, refer to available parts by looking at the elements in array 'allParts'

# SIFT-related (only required in case SIFT points need be recomputed)
sift_base_path = '../sift/'
sift_path = sift_base_path+'vlfeat-0.9.20/bin/glnxa64/'  # where SIFT binary is located. Change this if OS is not Linux
bin_sift = sift_path + 'sift'  # the binary itself

# tSNE-related (only required in case tSNE has to be tested)
tsne_path = '../tsne/'
tsne_python_path = tsne_path+'tsne_python/'  # where the Python code for tSNE is located.

######################################################
# end user preferences
######################################################



#####################################################
# General functionality
#####################################################

if 'tSNE' in partsToRun:
    sys.path.append(tsne_python_path)
    # check tSNE is available
    try:
        import tsne
    except ImportError:
        raise ImportError('To use tSNE, you are expected to first install it by running ' + tsne_path + 'install.sh')

def whatInWhatOut(what=None, dataIn=None, dataOut=None):
    if what is not None:
        n = len(what)
        print n*'-'
        print what
        print n*'-'
    if dataIn is not None:
        print "\t  INPUT:", dataIn
    if dataOut is not None:
        print "\t  OUTPUT:", dataOut

#####################################################
# Functions related to SIFT
#####################################################

def detectAndSaveSIFT(img_file, out_file):
    im = Image.open(img_file).convert('L')
    # make sure the file is PGM (convert it if it is not the case)
    if img_file[-3:] != 'pgm':
        img_file = 'tmp.pgm'
        im.save(img_file)

    # run sift
    edge_thres = config.SIFT_EDGE_THRES  # 10
    peak_thres = config.SIFT_PEAK_THRES  # 2
    params = "--edge-thresh " + str(edge_thres) + " --peak-thresh " + str(peak_thres)
    cmd = str(bin_sift + " " + img_file + " --output=" + out_file + " " + params)

    if bVerbose:
        print cmd
    os.system(cmd)


def generateSIFT(input_folder, output_folder):
    if not os.path.isdir(output_folder):
        print 'creating folder ' + output_folder
        os.makedirs(output_folder)
    fileNames = getFileNameList(input_folder)
    if bVerbose:
        print len(fileNames), "files found"
    for i, img_file in enumerate(fileNames):
        out_file = splitext(basename(img_file))[0] + '.dat'
        detectAndSaveSIFT(img_file, output_folder + '/' + out_file)


def generateAndSaveSIFTpoints():
    whatInWhatOut(what="Computing SIFT points",
                  dataIn=base_path + pnn_subfolder,
                  dataOut=base_path + sift_subfolder)

    if not os.path.isdir(sift_path):
        raise ImportError('To use SIFT, you are expected to first install it by running ' + sift_base_path + 'install.sh')
    sys.path.append(sift_path)  # make SIFT binary findable
    inputSubfolder = base_path + pnn_subfolder
    outputSubfolder = base_path + sift_subfolder
    for group in ['condi', 'sali', 'unpair']:
        generateSIFT(inputSubfolder + group, outputSubfolder + group)


######################################################
# end SIFT-related functions
######################################################


#####################################################
# Functions related to HND
#####################################################

def getFileNameList(folder, inputType='img', mice_ids=None):
    if inputType == 'img':
        ext = 'pgm'
    elif inputType == 'arr':  # numpy array
        ext = 'npy'
    else:  # 'feat'
        ext = 'dat'
    if mice_ids is None:
        if bVerbose:
            print folder + "/*." + ext
        fileList = glob.glob(folder + "/*." + ext)  # all image files, regardless of mouse id
    else:
        fileList = []
        for i, id in enumerate(mice_ids):
            fileList.extend(glob.glob(folder + "/*_mouse" + str(id) + "_*." + ext))  # only image files corresponding to given mice ids
    fileList.sort()  # so that the order is the same regardless of inputType (e.g. in case pairing images and arrays were desired)
    return fileList


# Here it is where the HND descriptor is actually computed from the given set of SIFT locations
def computeHistDists(sift_points, distThres=config.HND_DIST_THRES):
    bHisto = True

    k = config.HND_NUM_NEIGHBOURS
    N = config.HND_NUM_BINS_PER_HISTOGRAM  # num stats per nearest neighbor
    dims = k * N

    nPoints = sift_points.shape[0]
    if nPoints == 0:  # no SIFT points
        h = np.zeros((dims,))  # empty descriptor (all entries equal to 0)
        return h

    locs = sift_points[:, :2]  # get only x and y (we need the location of the SIFT point, not its descriptor)

    kmin = min(k, nPoints - 1)  # if less points than neighbors, reduce the number of neighbors

    nbrs = NearestNeighbors(n_neighbors=k + 1, algorithm='ball_tree').fit(locs)
    d, indices = nbrs.kneighbors(locs, n_neighbors=kmin + 1)
    d = d[:, 1:]  # exclude first column which is 0 (distance of each point to itself)

    h = np.zeros((N, k))

    thresh = 0  # -inf # 0
    for i in range(kmin):
        x = np.linspace(1, distThres, N + 1)
        h[:, i] = np.histogram(d[d[:, i] > thresh], x)[0]
    h = np.ravel(h)

    bNormalize = config.NORMALIZE_DESCRIPTOR
    if bNormalize:

        h = h.astype(float)  # to make sure the following division is not integer (which would result in 0's)
        if config.NORMALIZATION_TERM is 'num_points':
            h /= N
        else:
            s = np.sum(h)
            if s > 0:
                h /= s
    if bVerbose:
        print h
    return h


def loadSIFTandSaveHist(sift_file, out_file):
    if bVerbose:
        print 'loading SIFT file', sift_file
    P = np.loadtxt(sift_file)
    h = []

    distThresh = config.HND_DIST_THRES
    h.extend(computeHistDists(P, distThres=distThresh))

    if bVerbose:
        print 'saving histogram to ', out_file

    np.savetxt(out_file, h, fmt=config.FORMAT_BIN_COUNTS[config.NORMALIZE_DESCRIPTOR], newline=' ')


def generateHistogramFromSIFT(input_folder, output_folder):
    if not os.path.isdir(output_folder):
        if bVerbose:
            print 'creating folder ' + output_folder
        os.makedirs(output_folder)
    fileNames = getFileNameList(input_folder, 'feat')
    if bVerbose:
        print input_folder, ' has ', len(fileNames), ' files'
    for i, sift_file in enumerate(fileNames):
        out_file = splitext(basename(sift_file))[0] + '.dat'
        loadSIFTandSaveHist(sift_file, output_folder + '/' + out_file)


def generateAndSaveHND():
    whatInWhatOut(what="Computing HND descriptor",
                  dataIn=base_path + sift_subfolder,
                  dataOut=base_path + hnd_subfolder)


    inputSubfolder = base_path + sift_subfolder
    outputSubfolder = base_path + hnd_subfolder
    for group in ['condi', 'sali', 'unpair']:
        generateHistogramFromSIFT(inputSubfolder + group, outputSubfolder + group)


######################################################
# end HND-related functions
######################################################

######################################################
# tSNE-related functions
######################################################

def complementarySet(givenList, n):
    if type(givenList) is int:
        givenList = [givenList]  # make a list out of the (assumed) int number
    return list(set(range(n + 1)) - set(givenList) - set([0]))  # remove 0 because ids are 1-based


def loadHist(hist_file):
    return np.loadtxt(hist_file)


def loadDataFromFiles(folder, mice_ids=None, inputType='img', inputSubType='sift'):
    if False:  # not too verbose
        print 'folder ', folder

    fileList = getFileNameList(folder, inputType, mice_ids)

    nFiles = len(fileList)
    if nFiles == 0:
        return np.empty_like([]), np.empty_like([])

    if inputType == 'img':
        dims = Image.open(fileList[0]).size
    elif inputType == 'arr':
        dims = np.load(fileList[0]).shape
    else:  # inputType='feat'
        if inputSubType == 'sift':
            locs, descs = sc.loadSIFT(fileList[0])
            dims = descs.shape[1]
        elif inputSubType == 'hist':
            descs = loadHist(fileList[0])
            dims = descs.shape[0]
        else:
            pass  # TODO: for future subtypes...
        if False:  # not too verbose
            print dims

    # TODO: we may want to preprocess the image somehow (e.g. resize it)
    if inputType in ['img', 'arr']:  # for 2D data points
        h, w = dims[0], dims[1]

        bResize = True
        if bResize:
            h = w = 400

        bCrop = True

        if bCrop:
            hcrop = wcrop = 350  # final size, regardless it is either cropped or not
            assert (hcrop <= h and wcrop <= w)
        else:
            hcrop, wcrop = h, w

        X = np.empty((nFiles, 1, hcrop,
                      wcrop))  # 1 = for expanding 2D to 3D with singleton dim (so as to avoid problems in keras layers assuming 3-channel input images)
        A = np.zeros(nFiles, dtype=[('mouse_id', 'u1'), (
            'img_id', 'u1')])  # auxiliary information (A[i] info is associated to X[i]) # structured array

    else:  # 'feat'
        bResize = bCrop = False  # these operations do not make sense for feature vectors

        if inputSubType == 'sift':
            X = []  # we do not know the final size for X unless we process every image (since there are a variable number of data points per image)
            A = np.empty(0, dtype=[('mouse_id', 'u1'), ('img_id', 'u1'), ('x', 'u1'), ('y', 'u1')])  # we don't know final size..., so size=0
        else:  # TODO: this is for every 'feat' subtype where we only have one vector per image
            X = np.empty((nFiles, dims))
            A = np.zeros(nFiles, dtype=[('mouse_id', 'u1'), ('img_id', 'u1')])

    bVerbose = False
    for i in np.arange(nFiles):

        # extract mouse number and image number
        m = re.findall("[0-9]+", fileList[
            i])  # if more numbers can appear within the file path name, an alternative regular expression required
        mouseID, imgNum = int(m[0]), int(m[1])
        if bVerbose:
            print "file:", fileList[i]
            print 'mouse = ', mouseID, 'imgNum = ', imgNum
        if inputType == 'img':
            img = Image.open(fileList[i])
        elif inputType == 'arr':
            img = np.load(fileList[i])  # not actually an image (yet)
            if bResize or bCrop:  # convert to image only if required for image processing operations not available in numpy
                img = Image.fromarray(img)  # an actual Image now
        else:  # feats
            if inputSubType == 'sift':
                locs, descs = sc.loadSIFT(fileList[i])
            else:
                descs = loadHist(fileList[i])
        if bResize:
            img = img.resize((h, w))
        if bCrop:  # central cropping assumed, compute init point accordingly
            x0, y0 = int((w - wcrop) / 2), int((h - hcrop) / 2)
            img = img.crop((y0, x0, y0 + hcrop, x0 + wcrop))
        if bVerbose:
            imshow(img)
            show()
        if inputType in ['img', 'arr']:
            X[i] = np.expand_dims(np.array(img), axis=0)
            A[i] = np.array([(mouseID, imgNum)], dtype=A.dtype)
        else:  # 'feat'
            if inputSubType == 'sift':
                X.extend(descs)  # same idea to append, but behaves different and as we wish 
                nFeat = len(descs)
                A_i = np.zeros(nFeat, dtype=A.dtype)
                A_i['mouse_id'] = mouseID
                A_i['img_id'] = imgNum
                if nFeat > 0:  # if there is some feat
                    if bVerbose:
                        print 'mouse ', mouseID, 'image ', imgNum, 'has ', nFeat, ' features'
                    locs_arr = np.array(locs[:, 0:2])  # exclude scale and orientation (keep only x and y)
                    A_i['x'] = locs_arr[:, 0]
                    A_i['y'] = locs_arr[:, 1]
                    A = np.append(A, A_i)
            else:  # for feature vectors where there is only one per image
                X[i] = descs
                A[i] = np.array([(mouseID, imgNum)], dtype=A.dtype)

    if inputType == 'feat':
        X = np.array(X)  # convert list to numpy array
    if bVerbose:
        print 'sizes in loadDataFromFiles: ', X.shape, A.shape
    return X, A


def load_NeuronsUJI_dataset_byIDs(base_path, mice_ids_sali, mice_ids_condi, inputType, inputSubType):
    if False:  # not too verbose
        print inputType, inputSubType
    X_sali, A_sali = loadDataFromFiles(base_path + 'sali', mice_ids_sali, inputType, inputSubType)
    X_sali = X_sali.astype('float32')
    X_condi, A_condi = loadDataFromFiles(base_path + 'condi', mice_ids_condi, inputType, inputSubType)
    X_condi = X_condi.astype('float32')

    n1 = X_sali.shape[0]
    n2 = X_condi.shape[0]
    label_sali, label_condi = config.LABEL_SALI, config.LABEL_CONDI  # 0, 1
    y_sali = np.full((n1,), label_sali, dtype=float)
    y_condi = np.full((n2,), label_condi, dtype=float)
    if False:  # not too verbose
        print X_condi.shape, X_sali.shape

    bRequireSomeTraining = False  # to avoid having to have a training set

    if bRequireSomeTraining:
        if n1 == 0 and n2 == 0:
            print('Both the sali and condi list of images are empty. At least one of them should not')
            exit()

    if n1 == 0:
        X, y, A = X_condi, y_condi, A_condi
    elif n2 == 0:
        X, y, A = X_sali, y_sali, A_sali
    else:
        X = np.concatenate((X_sali, X_condi))
        y = np.concatenate((y_sali, y_condi))
        A = np.append(A_sali, A_condi)

    n = n1 + n2  # or X.shape[0]

    p = np.random.permutation(n)
    X = X[p]
    y = y[p]
    A = A[p]
    return X, y, A


def load_NeuronsUJI_dataset_splitByMiceIds(train_ids_sali, train_ids_condi, inputType, inputSubType):
    nMiceSali, nMiceCondi = config.NUM_MICE_SALI, config.NUM_MICE_CONDI
    if inputType is 'img':
        featTypeFolder = ''
    elif inputType is 'arr':
        featTypeFolder = '/sift_coded/'
    else:  # inputType is 'feat':
        if inputSubType == 'sift':
            featTypeFolder = sift_subfolder
        else:
            featTypeFolder = hnd_subfolder
    X_train, y_train, A_train = load_NeuronsUJI_dataset_byIDs(base_path + featTypeFolder, train_ids_sali,
                                                              train_ids_condi, inputType, inputSubType)
    test_ids_sali = complementarySet(train_ids_sali, nMiceSali)
    test_ids_condi = complementarySet(train_ids_condi, nMiceCondi)

    X_test, y_test, A_test = load_NeuronsUJI_dataset_byIDs(base_path + featTypeFolder, test_ids_sali, test_ids_condi,
                                                           inputType, inputSubType)
    return (X_train, y_train, A_train), (X_test, y_test, A_test)


def getDataForTSNE():
    mice_train_sali = range(1, config.NUM_MICE_SALI + 1)
    mice_train_condi = range(1, config.NUM_MICE_CONDI + 1)
    (X_train, y_train, A_train), (X_test, y_test, A_test) = load_NeuronsUJI_dataset_splitByMiceIds(mice_train_sali, mice_train_condi, 'feat', 'hist')
    if False:  # not too verbose
        print y_train.shape, X_train.shape, y_test.shape, X_test.shape

    return X_train, y_train, A_train


def plotTSNE(labels, Y, bSave=True, alpha=None):
    # color_per_label=zeros(labels.shape)
    # color_per_label[]
    plt.clf()
    spos = np.where(labels == config.LABEL_SALI)
    cpos = np.where(labels == config.LABEL_CONDI)
    # print spos,cpos
    if alpha is None:
        alpha = 0.5
    plt.scatter(Y[spos, 0], Y[spos, 1], 50, c='blue', marker='s', alpha=alpha, label="saline")  # c=color_per_label)
    plt.scatter(Y[cpos, 0], Y[cpos, 1], 50, c='red', marker='o', alpha=alpha, label="conditioned")  # c=color_per_label)
    plt.legend(loc='upper left')
    plt.grid()
    plt.xlabel('dim 1')
    plt.ylabel('dim 2')
    plt.title('t-SNE on HND descriptors')
    if bSave:
        plt.savefig(tsne_subfolder + 'tsne-saliVScondi.png')
    plt.show()


def do_tsne(n=2, perplexity=20):
    X, labels, A = getDataForTSNE()
    if False:
        print X.shape, labels.shape

    npoints = max(150, X.shape[0])  # min, max, depending on how many points we want to use
    X = X[:npoints, :]
    labels = labels[:npoints].astype(int)

    if False:
        print X.shape, labels.shape
        print np.unique(labels)

    Y = tsne.tsne(X, n, X.shape[1], perplexity)
    if False:
        print Y.shape

    plotTSNE(labels, Y, bSave=True)

    bSave = True
    if bSave:
        suffix = "P" + str(perplexity)
        f = open(tsne_subfolder + 'tsne' + '_' + suffix + '.pkl', 'wb')
        data = {'X': X, 'labels': labels, 'A': A, 'Perp': perplexity, 'Y': Y}

        pickle.dump(data, f)
        f.close()


def labelPoints(ax, points, Y, firstId=1):
    n = len(points)
    for i in range(len(points)):
        x = Y[points[i], 0]
        y = Y[points[i], 1]
        id = i + firstId
        print "Point: ", id, "is at (x,y) ", x, y
        # plt.plot(x, y, 'bo')
        ax.text(x + 0.5, y + 0.5, id, fontsize=15)


def ploTSNESelected(labels, Y, group, text, loc, zoomLines):
    plt.clf()
    spos = np.where(labels == config.LABEL_SALI)
    cpos = np.where(labels == config.LABEL_CONDI)

    fig, ax = plt.subplots()
    ax.scatter(Y[spos, 0], Y[spos, 1], 50, c='gray', marker='s', alpha=0.2)
    ax.scatter(Y[cpos, 0], Y[cpos, 1], 50, c='gray', marker='o', alpha=0.2)

    s_and_g = np.array(list(set(spos[0]).intersection(set(group))))  # saline AND selected group
    c_and_g = np.array(list(set(cpos[0]).intersection(set(group))))  # saline AND selected group

    all_x = [] if len(s_and_g) == 0 else Y[s_and_g, 0]
    all_x = np.append(all_x, [] if len(c_and_g) == 0 else Y[c_and_g, 0])
    if bVerbose:
        print all_x

    all_y = [] if len(s_and_g) == 0 else Y[s_and_g, 1]
    all_y = np.append(all_y, [] if len(c_and_g) == 0 else Y[c_and_g, 1])
    if bVerbose:
        print all_x, all_y

    if len(all_x) == 0:
        return

    xmin, xmax = min(all_x), max(all_x)
    ymin, ymax = min(all_y), max(all_y)

    xmin = xmin - 5
    ymin = ymin - 5
    xmax = xmax + 5
    ymax = ymax + 5

    bZoomIn = zoomLines is not None  # True
    bLabelPoints = bZoomIn
    if bZoomIn:
        location = loc
        zoomFactor = 3
        axZoom = zoomed_inset_axes(ax, zoomFactor, loc=location)
        axZoom.clear()
        x1, x2, y1, y2 = xmin, xmax, ymin, ymax  # specify the limits
        axZoom.set_xlim(x1, x2)  # apply the x-limits
        axZoom.set_ylim(y1, y2)  # apply the y-limits
        axZoom.scatter(Y[spos, 0], Y[spos, 1], 50, c='gray', marker='s', alpha=0.2)
        axZoom.scatter(Y[cpos, 0], Y[cpos, 1], 50, c='gray', marker='o', alpha=0.2)

        mark_inset(ax, axZoom, loc1=zoomLines[0], loc2=zoomLines[1], fc="none")

    axWhereToDrawPoints = axZoom if bZoomIn else ax
    if len(s_and_g) > 0:
        ax.scatter(Y[s_and_g, 0], Y[s_and_g, 1], 50, c='blue', marker='s', alpha=0.9)
        if bZoomIn:
            axWhereToDrawPoints.scatter(Y[s_and_g, 0], Y[s_and_g, 1], 50, c='blue', marker='s', alpha=0.9)
        if bLabelPoints:
            n = labelPoints(axWhereToDrawPoints, s_and_g, Y)

    if len(c_and_g) > 0:
        ax.scatter(Y[c_and_g, 0], Y[c_and_g, 1], 50, c='red', marker='o', alpha=0.9)
        if bZoomIn:
            axWhereToDrawPoints.scatter(Y[c_and_g, 0], Y[c_and_g, 1], 50, c='red', marker='o', alpha=0.9)
        if bLabelPoints:
            labelPoints(axWhereToDrawPoints, c_and_g, Y, firstId=len(s_and_g) + 1)

    ax.set_xlabel('dim 1')
    ax.set_ylabel('dim 2')
    ax.set_title('t-SNE on HND descriptors')
    ax.grid()

    plt.savefig(tsne_subfolder + text + '.png')
    plt.show()


def showImagesFromSelectedGroup(A, labels, Y, group, text, loc, zoomLines, bExtractImgs=True, nImgs=None):
    if bVerbose:
        print "Selected images for group", text

    if nImgs is not None:
        p = np.random.permutation(group[0])
        group = p[:nImgs]
    else:
        group = group[0]

    selectedImgs = zip(A[group]['mouse_id'], A[group]['img_id'], labels[group])

    ploTSNESelected(labels, Y, group, text, loc, zoomLines)  # plot TSNE output with selected points highlighted

    if not bExtractImgs:
        return
    i = 0
    for mouse, img, condi in selectedImgs:
        if bVerbose:
            print mouse, img, condi
        img_file = ('sali' if condi == config.LABEL_SALI else 'condi') + '/mouse' + str(mouse) + '_img' + str(
            img) + '.png'
        print "Selected image:", img_file
        print "\t Dim 1 in t-SNE", Y[group[i], 0]
        print "\t Dim 2 in t-SNE", Y[group[i], 1]
        i += 1
    plt.clf()


def analyseTSNE(P):
    suffix = "_paper" if bUseTSNEfromPaper else ""
    f = open(tsne_subfolder + 'tsne_P' + str(P) + suffix + '.pkl')
    data = pickle.load(f)
    X, labels, A, Y, P = data['X'], data['labels'], data['A'], data['Y'], data['Y']

    plotTSNE(labels, Y, bSave=True)  # output similar to Fig. 4

    # Selecting distinct areas in tSNE space (for an output similar to Fig. 5)
    # This only makes sense for a particular output, since it uses information found
    # by manual inspection of that output.
    # Therefore, if you want to test this, please identify first the areas for a particular output
    # and then define the appropriate arguments for showImagesFromSelectedGroup()
    bAreas = True
    if bAreas:
        LOCS = {'upper-left': 2, 'upper-center': 9}
        showImagesFromSelectedGroup(A, labels, Y, np.where(Y[:, 1] > 60),
                                    "dense", loc=LOCS['upper-center'], zoomLines=[1, 4],
                                    nImgs=5)  # selection by looking interactively at the plot

        showImagesFromSelectedGroup(A, labels, Y, np.where(np.logical_and(Y[:, 0] < -50, Y[:, 1] > -15)),
                                    "loose", loc=LOCS['upper-center'], zoomLines=[2, 4],
                                    nImgs=5)  # selection by looking interactively at the plot

        showImagesFromSelectedGroup(A, labels, Y,
                                    np.where(np.logical_and(Y[:, 1] > 0, np.logical_and(Y[:, 0] > -20, Y[:, 0] < 0))),
                                    "mid", loc=LOCS['upper-center'], zoomLines=[3, 4],
                                    nImgs=5)  # selection by looking interactively at the plot

    # selecting distinct mice in tSNE space (output similar to Fig. 6)
    bMice = True
    if bMice:
        selectedMouse = 1
        selectedGroup = config.LABEL_CONDI
        showImagesFromSelectedGroup(A, labels, Y,
                                    np.where(np.logical_and(A['mouse_id'] == selectedMouse, labels == selectedGroup)),
                                    "mice" + str(selectedMouse) + "_" + config.LABELS_NAME[selectedGroup], loc=None,
                                    zoomLines=None,
                                    bExtractImgs=False)  # selection by looking interactively at the plot
        selectedMouse = 1
        selectedGroup = config.LABEL_SALI
        showImagesFromSelectedGroup(A, labels, Y,
                                    np.where(np.logical_and(A['mouse_id'] == selectedMouse, labels == selectedGroup)),
                                    "mice" + str(selectedMouse) + "_" + config.LABELS_NAME[selectedGroup], loc=None,
                                    zoomLines=None,
                                    bExtractImgs=False)  # selection by looking interactively at the plot


def generateAndAnalyseHND():

    print tsne_python_path

    whatInWhatOut(what="Computing HND descriptor",
                  dataIn=base_path + hnd_subfolder,
                  dataOut=tsne_subfolder)

    n = 2  # number of ouput dimensionality
    P = 20  # perplexity value
    # generate (and save output)
    if bUseTSNEfromPaper:
        P = 20  # force value used in paper (n=2 but not required for file loading)
    else:
        if not os.path.isdir(tsne_python_path):
            print "Please, make sure the tSNE Python code is available at", tsne_python_path
            print "Remember you are expected to install tSNE by running ", tsne_path+'install.sh'
        do_tsne(n, P)
    # read output and display
    analyseTSNE(P)


######################################################
# end tSNE-related functions
######################################################

######################################################
# KDE-related functions
######################################################

def writeSelectedSamples(cluster, idx, A, y):

    filename=kde_subfolder+"cluster"+str(cluster)+".dat"
    f = open(filename,"w")
    f.write("group\t mouse\t image\t \n")
    for i in idx:
        ii = i[0]
        if False: # not too Verbose
            print "image", A['img_id'][ii], "from mouse ", A['mouse_id'][ii], "of class",  y[ii]
        f.write(config.VERY_SHORT_LABELS_NAME[y[ii]]+"\t"+str(A['mouse_id'][ii])+"\t"+str(A['img_id'][ii])+"\n")
    f.close()

    with open(filename,'r') as f:
        lines = f.readlines()

    print "These are the PNN images corresponding to selected HND for 'pattern' (cluster)", str(cluster)+":"
    for line in lines[1:]: # exclude first line (the header)
        info = line.split("\t")
        group, mouse_id, img_id =info[0], info[1], info[2][:-1] # exclude last char (i.e. the EOF char) from last element,
        subpath='condi' if group=="c" else "sali"
        print "\t - "+subpath+"/mouse"+mouse_id+"_img"+str(img_id)+".png"

def sampleCluster(data,n=config.KDE_NUM_POINTS_PER_CLUSTER):
    # Estimate a density function (gaussian kernel and automatic bandwith selection)
    # https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.stats.gaussian_kde.html
    data = data + 0.001*np.random.randn(data.shape[0],data.shape[1]) # to avoid the error "numpy.linalg.linalg.LinAlgError: singular matrix"
    kernel = stats.gaussian_kde(data.T) # transpose data as required here

    # Compute the density on the given data
    density = kernel.evaluate(data.T)

    # Sample the data proportionally to its density (return the indices to the data, not new data)
    density = density/sum(density) # normalize to sum 1, as required by np.randon.choice()
    m = density.shape[0] # num of data points
    idx = np.random.choice(range(m), size=n, p=density)

    return idx

def analyzeClusters(X, y, A, kmeans):
    assignments = kmeans.predict(X)
    h_cluster_class = {}  # dict.fromkeys()np.zeros((kmeans.n_clusters,np.unique(y)))
    for cluster in range(kmeans.n_clusters):
        idx_cluster = np.where(assignments == cluster)
        data = X[idx_cluster]
        idx_sample = sampleCluster(data)
        if False: # not too verbose
            print "indices of sampled data within cluster", idx_sample
        idx_cluster_asArray = np.asarray(idx_cluster).T
        original_idx = idx_cluster_asArray[idx_sample]
        writeSelectedSamples(cluster, original_idx, A, y)
        # raw_input("sampled cluster...")
        if bVerbose:
            print data.shape[0], "points in cluster", cluster
        for classLabel in np.unique(y):
            data_perClass = X[np.where(np.logical_and(assignments == cluster, y == classLabel))]
            h_cluster_class[cluster, classLabel] = data_perClass.shape[0]
            if bVerbose:
                print "  * ", data_perClass.shape[0], "points of class (group)", config.LABELS_NAME[classLabel]
        D = pairwise_distances(data, metric='euclidean')
        if False: # not too verbose
            print D.shape
            print D

    if False:  # not too verbose
        for cluster in range(kmeans.n_clusters):
            print "cluster ", cluster, ":"
            for classLabel in np.unique(y):
                if bVerbose:
                    print "\t class", classLabel, ":", h_cluster_class[cluster, classLabel]

def test_kde(id_test=1, nClusters=5, bReNormalize=False):
    whatInWhatOut(what="Clustering HND and sampling instances per cluster",
                  dataIn=base_path+hnd_subfolder,
                  dataOut=kde_subfolder)

    if bReNormalize:
        config.NORMALIZE_DESCRIPTOR = True
        config.NORMALIZATION_TERM = 'sum'
        generateAndSaveHND()
    if id_test <= config.NUM_MICE_SALI:
        mice_test_sali = id_test
        mice_train_sali = complementarySet(mice_test_sali, config.NUM_MICE_SALI)
        mice_test_condi = []
        mice_train_condi = complementarySet(mice_test_condi, config.NUM_MICE_CONDI)
    else:
        mice_test_condi = id_test - config.NUM_MICE_SALI
        mice_train_condi = complementarySet(mice_test_condi, config.NUM_MICE_CONDI)
        mice_test_sali = []
        mice_train_sali = complementarySet(mice_test_sali, config.NUM_MICE_SALI)

    (X_train, y_train, A_train), (X_test, y_test, A_test) = load_NeuronsUJI_dataset_splitByMiceIds(mice_train_sali, mice_train_condi, 'feat', 'hist')

    kmeans = KMeans(n_clusters=nClusters)
    kmeans.n_clusters

    # for the expert analysis (Section 3.1), we include both training and test data
    X = np.vstack((X_train, X_test))
    y = np.append(y_train, y_test)
    A = np.concatenate((A_train, A_test))
    if False:  # not too verbose
        print "A_train.shape", A_train.shape
        print "X.shape", X.shape
        print "y.shape", y.shape
        print "A.shape", A.shape

    kmeans.fit(X)
    analyzeClusters(X, y, A, kmeans)

######################################################
# end KDE-related functions
######################################################


######################################################
# BoW and SVM-related functions
######################################################

def preprocessDataset(X, Xmean=None):
    doubleReturn = (Xmean is None)

    # compute training mean if not provided
    if Xmean is None:
        Xmean = X.mean(axis=0)

    X = X - Xmean  ## subtract train mean

    if doubleReturn:
        return X, Xmean
    else:
        return X, []  # if mean was provided, do not return it :)


def normalizeDataset(X):
    num_rows, num_cols = X.shape
    l2 = np.linalg.norm(X, 2, axis=1)
    l2 = np.repeat(np.reshape(l2, (num_rows, 1)), num_cols, axis=1)
    X /= l2  # L2 normalization
    return X


def preprocessDataTraTe(X_train, X_test):
    # Normalize data
    (X_train, Xmean_train) = preprocessDataset(X_train)
    X_test = preprocessDataset(X_test, Xmean_train)[0]

    ## normalize
    bNormalize = True
    if bNormalize:
        X_train = normalizeDataset(X_train)
        X_test = normalizeDataset(X_test)

    return X_train, X_test, Xmean_train


def selectSVM(X, y):
    X_train, y_train = X, y

    Crange = [10 ** j for j in range(-8, 8, 1)]
    gammaRange = [1e+1, 1e+2, 1e+3, 1e+4]
    gammaRange = gammaRange + [1.0 / x for x in gammaRange] + [1]
    params_range_RBF = {'kernel': ['rbf'], 'gamma': gammaRange, 'C': Crange}
    params_range_linear = {'kernel': ['linear'], 'C': Crange}
    params_range = []
    params_range.append(params_range_RBF)

    score = 'accuracy'
    if False:  # not too verbose
        print("# Tuning hyper-parameters for %s" % score)

    class_weight = 'balanced'

    clf = GridSearchCV(svm.SVC(C=1, class_weight=class_weight), params_range, cv=5, scoring='%s' % score, n_jobs=10)
    clf.fit(X_train, y_train)

    if False:  # not too verbose
        print("Best parameters set found on development set:")
        print(clf.best_params_)

        rangesInfo = "range: " + str(params_range) + "\n" + \
                     "best params: " + str(clf.best_params_) + "\n" + \
                     "validation time: " + str(timedelta(seconds=t)) + "\n"
        print rangesInfo

    if False:  # not too verbose
        print("Grid scores on development set:")
        print()
        means = clf.cv_results_['mean_test_score']
        stds = clf.cv_results_['std_test_score']
        for mean, std, params in zip(means, stds, clf.cv_results_['params']):
            print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))

        print("Detailed classification report:")
        print("The model is trained on the full development set.")
        print("The scores are computed on the full evaluation set.")

        print clf.get_params()
        print "Best params classifier", clf.best_params_

    return clf.best_estimator_


def trainAndClassify(X_train, y_train, X_test, y_test):
    # Preprocess data
    if False:  # not too verbose
        print X_train.shape, X_test.shape
    X_train, X_test, Xmean_train = preprocessDataTraTe(X_train, X_test)

    # Convert data format
    ## column vector -> 1D array (as expected by fit())
    y_train = np.ravel(y_train)  # shape before is (n,1) and after is (n,)
    y_test = np.ravel(y_test)
    if False:  # not too verbose
        print y_train.shape
        print y_test.shape

    # Learn classifier
    clf = selectSVM(X_train, y_train)

    # Test classifier
    y_test_pred = clf.predict(X_test)

    return clf, y_test_pred, Xmean_train


def bow(X, y, A, kmeans):
    nClusters = kmeans.n_clusters
    if False:  # not too verbose
        print X.shape, y.shape, A.shape
    mice = np.unique(A['mouse_id'])
    s, c, u = config.LABEL_SALI, config.LABEL_CONDI, config.LABEL_UNCERTAIN

    # for each mouse id
    # X_bow = np.empty(shape=(1,nClusters))
    X_bow = y_bow = np.array([])
    # The auxiliary information now does not have the img_id, because the BOW is over the images and we end up with a single histogram per mouse
    A_bow = np.empty(0, dtype=[('mouse_id',
                                'u1')])  # , ('img_id', 'u1')])  # auxiliary information (A[i] info is associated to X[i]) # structured array

    for j, mouse in enumerate(mice):
        idx_mouse = np.where(A['mouse_id'] == mouse)
        # for each experimental group (saline, conditioned...). Note: this is required since we have repeated mouse ids for the different groups
        for group in np.unique(y[idx_mouse]):
            if False:  # not too verbose
                print "Mouse", mouse, " group", config.LABELS_NAME[group]

            # find descriptors corresponding to mouse 'idx_mouse' in group 'group' (one descriptor per image)
            which = np.where(y[idx_mouse] == group)[0]
            data = X[which]

            # find the clusters for this set of descriptors
            clusters_assignments = kmeans.predict(data)

            if False:  # not too verbose
                print len(clusters_assignments)  # number of images for this (mouse,group) pair

            # compute histogram
            h, bin_edges = np.histogram(clusters_assignments, bins=range(nClusters + 1),
                                        density=True)  # L1 normalized if density=True
            if X_bow.shape[0] == 0:
                X_bow = h
            else:
                X_bow = np.vstack((X_bow, h))
            y_bow = np.append(y_bow, group)
            A_i = np.zeros(1, dtype=A_bow.dtype)
            A_i['mouse_id'] = mouse
            A_bow = np.append(A_bow, A_i)

    if len(X_bow.shape) == 1:  # turn 1-dim into 2-dim, i.e. (n,) -> (n,1)
        X_bow = X_bow.reshape((1, X_bow.shape[0]))

    if False:  # not too verbose
        print X_bow, y_bow, A_bow['mouse_id']
        print X_bow.shape, y_bow.shape

    return X_bow, y_bow, A_bow

def test_bow(id_test=1, nClusters=5, bReNormalize=False):
    if bReNormalize:
        config.NORMALIZE_DESCRIPTOR = True
        config.NORMALIZATION_TERM = 'sum'
        generateAndSaveHND()
    if id_test <= config.NUM_MICE_SALI:
        mice_test_sali = id_test
        mice_train_sali = complementarySet(mice_test_sali, config.NUM_MICE_SALI)
        mice_test_condi = []
        mice_train_condi = complementarySet(mice_test_condi, config.NUM_MICE_CONDI)
    else:
        mice_test_condi = id_test - config.NUM_MICE_SALI
        mice_train_condi = complementarySet(mice_test_condi, config.NUM_MICE_CONDI)
        mice_test_sali = []
        mice_train_sali = complementarySet(mice_test_sali, config.NUM_MICE_SALI)

    (X_train, y_train, A_train), (X_test, y_test, A_test) = load_NeuronsUJI_dataset_splitByMiceIds(mice_train_sali, mice_train_condi, 'feat', 'hist')

    kmeans = KMeans(n_clusters=nClusters)
    kmeans.n_clusters


    # compute kmeans only with training data for classification with the test set
    kmeans.fit(X_train)

    if False:  # not too verbose
        print kmeans.labels_
        print kmeans.cluster_centers_
        assignments1 = kmeans.predict(X_test)

    # Actual required work: "Training BOWs"

    X_bow_train, y_bow_train, A_bow_train = bow(X_train, y_train, A_train, kmeans)

    if False:  # not too verbose
        print "histograms of BoW (train)"
        print X_bow_train[np.where(y_bow_train == config.NUM_MICE_CONDI)]
        print "----"
        print X_bow_train[np.where(y_bow_train == config.NUM_MICE_SALI)]

    # print "Test BOWs"
    X_bow_test, y_bow_test, A_bow_test = bow(X_test, y_test, A_test, kmeans)

    if False:  # not too verbose
        print "histograms of BoW (train)"
        print X_bow_test

    clf, y_bow_test_pred, Xmean_bow_train = trainAndClassify(X_bow_train, y_bow_train, X_bow_test, y_bow_test)

    accuracy = np.mean(y_bow_test == y_bow_test_pred) * 100

    return accuracy, y_bow_test, y_bow_test_pred


def do_test_withUnpair(numClusters=None):
    if numClusters == None:
        clusterSizes = config.NUM_CLUSTERS_UNPAIR_TEST
    else:
        clusterSizes = [numClusters]
    numRep = config.NUM_REPS

    for nClusters in clusterSizes:
        a = {}
        a[nClusters] = []
        for rep in range(numRep):
            print '* * * testing nClusters = ', nClusters, "repetition", rep
            a[nClusters] = np.append(a[nClusters], test_bow_withUnpair(nClusters))
            savePerformance(a, nClusters, "_unpair")
        print "accuracy for each repetition:", a[nClusters]


def testWithUnpair():
    for k in NUM_CLUSTERS_UNPAIR_TEST:
        do_test_withUnpair(k)
        do_report(k, suffix="_unpair")


def do_test(numClusters=None, bSave=True, bPrint=True):
    a = {}
    if numClusters == None:
        clusterSizes = config.NUM_CLUSTERS
    else:
        clusterSizes = [numClusters]
    numRep = config.NUM_REPS
    test_mouse_set = range(1, config.NUM_MICE_CONDI + config.NUM_MICE_CONDI + 1)

    bFirstCall = True

    labels2Index={config.LABEL_SALI:0, config.LABEL_CONDI:1}
    for nClusters in clusterSizes:
        cm=np.zeros((2,2)) # cumulative confusion matrix for the binary case (2 classes: saline and conditioned)
        for id_test in test_mouse_set:
            a[nClusters, id_test] = []
            for i in range(numRep):
                if bVerbose:
                    print '* * * testing: nClusters = ' + str(nClusters) + ", mouse id = " + str(
                        id_test) + ", repetition", i
                acc, yTrue, yPred = test_bow(id_test, nClusters, bReNormalize=False)  # bReNormalize=bFirstCall
                a[nClusters, id_test] = np.append(a[nClusters, id_test], acc)
                yTrue,yPred = yTrue[0],yPred[0] # we are assuming single-element 1D arrays whose unique (first) element is of our interest
                trueClass = config.LABEL_SALI if id_test <= config.NUM_MICE_SALI else config.LABEL_CONDI
                cm[labels2Index[yTrue],labels2Index[yPred]] += 1

                bFirstCall = False  # not the first call anymore
            if bSave:
                savePerformance(a, nClusters)
            if bPrint:
                print "Hit/miss (one per repetition) for mouse id " + str(id_test) + " and " + str(nClusters) + " clusters:", a[nClusters, id_test]

        if bConfusionMatrix:
            print "Confusion matrix"
            print cm
            print "Confusion matrix in % accross rows"
            print 100*cm/np.sum(cm,axis=1)
            print "Overall accuracy:",100 * np.sum(np.diag(cm)) / np.sum(cm)

def savePerformance(a, k, suffix=""):
    paperSuffix = "_paper" if bUseAccFromPaper else ""
    f = open(accu_subfolder + 'accuracy' + '_' + str(k) + suffix + paperSuffix + '.pkl', 'wb')
    pickle.dump(a, f)
    f.close()


def reportPerformanceUnpair(a):
    k = a.keys()
    clusterSizes = np.unique([x for x in k])

    for nClusters in clusterSizes:
        data = a[nClusters]
        print "\t - For nClusters =", str(nClusters) + ", Accuracy (%) over", len(data), "repetitions: median = ", \
            np.median(data), "mean = ", np.mean(data), " std. dev", np.std(data)


def id2ref_str(id_mouse):
    msg = "("
    if id_mouse <= config.NUM_MICE_SALI:
        group = config.LABEL_SALI
    else:
        group = config.LABEL_CONDI
        id_mouse -= config.NUM_MICE_SALI

    msg = "(id: " + str(id_mouse) + ", group: " + config.LABELS_NAME[group] + ")"
    return msg


def reportPerformance(a):
    k = a.keys()
    mouse_ids = np.unique([x[1] for x in k])
    clusterSizes = np.unique([x[0] for x in k])

    for nClusters in clusterSizes:
        averages = np.zeros((3, 1))
        mean_idx, stdev_idx, median_idx = 0, 1, 2
        for id_test in mouse_ids:
            if bVerbose:
                print "* For mouse_id (test):", str(id_test) + " " + id2ref_str(id_test)
            data = a[nClusters, id_test]
            median = np.median(data)
            mean = np.mean(data)
            stddev = np.std(data)
            averages[mean_idx] += mean
            averages[stdev_idx] += stddev
            averages[median_idx] += median
            print "\t - For nClusters =", nClusters, ", Accuracy (%) over", len(data), "repetitions: median = ", str(
                median) + ", mean = " + str(mean) + ", std. dev. =", stddev
        averages = np.divide(averages, len(mouse_ids))
        print "*** For nClusters =", nClusters, " Averaged median,mean,stddev (%) over ", len(
            mouse_ids), "mice: median = ", averages[median_idx], "mean = ", averages[mean_idx], " std. dev", averages[
            stdev_idx]


def readPerformance(file):
    f = open(file, 'r')
    if bVerbose:
        print "Reading", file
    a = pickle.load(f)
    f.close()
    return a


def do_report(k=None, suffix=""):
    if k is None:
        if suffix == "_unpair":
            clusters = config.NUM_CLUSTERS_UNPAIR_TEST
        else:
            clusters = config.NUM_CLUSTERS
    else:
        clusters = [k]

    for k in clusters:
        paperSuffix = "_paper" if bUseAccFromPaper else ""
        a = readPerformance(accu_subfolder + 'accuracy' + '_' + str(k) + suffix + paperSuffix + '.pkl')
        if suffix == "_unpair":
            reportPerformanceUnpair(a)
        else:
            reportPerformance(a)


def setExpectedLabel(m, expected_label):
    return np.full((m), expected_label, dtype=int)


def getDataUnpaired(inputType, inputSubType):
    mice_ids_unpair = range(1, config.NUM_MICE_UNPAIR + 1)
    if inputSubType is 'hist':
        subTypefolder = base_path + hnd_subfolder
    X_unpair, A_unpair = loadDataFromFiles(subTypefolder + '/unpair', mice_ids_unpair, inputType, inputSubType)

    X_unpair = X_unpair.astype('float32')
    m = X_unpair.shape[0]
    y_unpair = setExpectedLabel(m, config.LABEL_SALI)  # The "true" class of "unpair" assumed to be saline

    return X_unpair, y_unpair, A_unpair


def test_bow_withUnpair(nClusters):
    # all mice in saline and conditioned groups are used for training
    # We leave none for testing; we test on unpaired group
    mice_train_sali = range(1, config.NUM_MICE_SALI)
    mice_train_condi = range(1, config.NUM_MICE_CONDI)
    (X_train, y_train, A_train), (X_test, y_test, A_test) = load_NeuronsUJI_dataset_splitByMiceIds(mice_train_sali, mice_train_condi, 'feat', 'hist')

    # actual test test set with unpaired mice
    X_unpair, y_unpair, A_unpair = getDataUnpaired(inputType='feat', inputSubType='hist')

    kmeans = KMeans(n_clusters=nClusters)

    kmeans.fit(X_train)

    X_bow_train, y_bow_train, A_bow_train = bow(X_train, y_train, A_train, kmeans)

    X_bow_unpair, y_bow_unpair, A_bow_unpair = bow(X_unpair, y_unpair, A_unpair, kmeans)

    clf, y_bow_unpair_pred, Xmean_bow_train = trainAndClassify(X_bow_train, y_bow_train, X_bow_unpair, y_bow_unpair)

    accuracy = np.mean(y_bow_unpair == y_bow_unpair_pred) * 100
    if False:  # not too verbose
        print "predicted labels:", y_bow_unpair_pred
        print "true labels:", y_bow_unpair

    return accuracy


def BoWandSVM():
    whatInWhatOut(what="Clustering HND, learning SVM and classifying mice",
                  dataIn=base_path + hnd_subfolder,
                  dataOut=accu_subfolder)

    # Test with saline and conditioned group
    if not bUseAccFromPaper:
        do_test(bPrint=bVerbose)
    do_report()

    # Test with unpaired group
    if bAlsoUnpair:
        if not bUseAccFromPaper:
            do_test_withUnpair()
        do_report(suffix="_unpair")


######################################################
# end BoW and SVM-related functions
######################################################

if __name__ == '__main__':

    # Mentioned in Section 2.4 (SIFT points locations are required for HND computation)
    if 'sift' in partsToRun:
        generateAndSaveSIFTpoints()

    # Section: 2.4 Image descriptor (HND)
    if 'hnd' in partsToRun:
        generateAndSaveHND()

    # Section: 2.5.1 Visualizing the HND in a low-dimensional space
    # Fig. 4, 5, 6
    if 'tSNE' in partsToRun:
        generateAndAnalyseHND()

    # Section: 2.5.3 Choosing representative images
    # Section: 3.1 Qualitative assessment of discovered PNN patterns
    if 'kde' in partsToRun:
        test_kde(nClusters=config.KDE_NUM_CLUSTERS)

    # Section: 2.5.2 Finding image patterns unsupervisedly
    # Section: 2.5.4 Recognizing the mice group supervisedly
    # Section: 3.2 Quantifying the mice group recognition rate
    if 'bow+svm' in partsToRun:
        BoWandSVM()
