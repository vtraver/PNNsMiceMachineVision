######################################################################
# SIFT detector (and descriptor)
######################################################################

SIFT_EDGE_THRES = 10
SIFT_PEAK_THRES = 2

######################################################################
# HND descriptor
######################################################################

HND_DIST_THRES = 30 # Distance around every point to consider
HND_NUM_NEIGHBOURS = 5 # Number of histograms to consider
HND_NUM_BINS_PER_HISTOGRAM = 3 # Number of bins per histogram

NORMALIZE_DESCRIPTOR=True

if NORMALIZE_DESCRIPTOR:
    NORMALIZATION_TERM='num_points'
    NORMALIZATION_TERM='sum'
else:
    NORMALIZATION_TERM=None

FORMAT_BIN_COUNTS = {True:'%g', False:'%d'}


######################################################################
# group labels and validation details
######################################################################

LABEL_SALI, LABEL_CONDI = -1,1
LABEL_UNDEF = 100
LABEL_UNCERTAIN = 0
LABELS_NAME = {LABEL_CONDI:'conditioned', LABEL_SALI:'saline'}
VERY_SHORT_LABELS_NAME = {LABEL_UNCERTAIN:'u', LABEL_CONDI:'c', LABEL_SALI:'s', LABEL_UNDEF:'0'}
NUM_MICE_SALI, NUM_MICE_CONDI = 6,6
NUM_MICE_UNPAIR = 5
NUM_REPS = 10
NUM_CLUSTERS=[2, 3, 4, 5]
NUM_CLUSTERS_UNPAIR_TEST = [2,3,4,5,6,8,10,15,20,50,100]
LABELS_NAME = {LABEL_UNCERTAIN:'uncertain', LABEL_CONDI:'conditioned', LABEL_SALI:'saline', LABEL_UNDEF:'undefined'}

######################################################################
# For KDE-based sampling
######################################################################
KDE_NUM_CLUSTERS=3
KDE_NUM_POINTS_PER_CLUSTER=10