image=../../data/PNNimages/condi/img_mouse1_17.pgm
jetColorMap=yes # or no 
python saveImagesInColor.py ${image} ${jetColorMap}
echo " * * * * * * * *  Output images:"
ls -lt ../../outputs/imgs/out*png
echo "You can display them with your prefered software. For instance, from command line"
echo "display ../../outputs/imgs/out.png"
echo "display ../../outputs/imgs/out_with_bar.png"
echo "Please, rename the output files if desired"

