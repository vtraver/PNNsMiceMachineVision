import os
from numpy import loadtxt
from pylab import *
from PIL import Image

DOT_SIZE = 8


def loadSIFT(sift_file):
    # read detected STIPS and descriptors
    fd = open(sift_file, 'r')
    nbytes = os.fstat(fd.fileno()).st_size
    fd.close()
    if nbytes:  # make sure the file is not empty
        f = loadtxt(sift_file)
        locations = f[:, :4]  # x,y,scale,rotation
        descriptors = f[:, 4:]  # 128-D descriptor
    else:  # if file empty (i.e., no STIP detected)
        print 'no STIP detected'
        locations = []
        descriptors = []

    return locations, descriptors


def drawSIFTpoints(img_file, sift_filename, out_filename, bShowImg=True):
    locs, descs = loadSIFT(sift_file)
    if len(locs) > 0:
        xy = locs[:, :2]
        plot(xy[:, 0], xy[:, 1], 'yo', markersize=DOT_SIZE)
    if bShowImg:
        imshow(array(Image.open(img_file)))

    savefig(out_filename)
    show()
    plt.gcf().clear()


if __name__ == "__main__":
    # usage example

    # identify PNN image to use by providing group name, and mouse and image identifiers
    group = 'condi'
    mouse_id = 1
    img_id = 7
    mouse_img = 'img_mouse' + str(mouse_id) + '_' + str(img_id)

    # input PNN image to display the SIFT points on
    img_file = '../../data/PNNimages/' + group + '/' + mouse_img + '.pgm'

    # input text file with the detected SIFT points on the PNN image
    sift_file = '../../data/SIFTpoints/' + group + '/' + mouse_img + '.dat'

    # output PNN image with the SIFT points overlaid
    out_file = '../../outputs/' + group + '_' + mouse_img + '_withSIFT.png'

    # actual task: draw SIFT points (sift_file) on PNN image (img_file)
    # and write the resulting image (out_file)
    drawSIFTpoints(img_file, sift_file, out_file, True)  # last argument is whether to display the resulting image

    # let the user know where the output image has been left
    print "Output image available at", out_file