#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
import matplotlib.image as pltimg
import sys

base_path='../../outputs/imgs/' # for output file names

if len(sys.argv)<2: # 1st parameter is image file path
	print "Please, provide the path to image file "
	exit(1)

name = sys.argv[1]

if len(sys.argv)==3: # 2nd parameter, optional, is whether to use jet colormap
	if sys.argv[2] in ['y', 'Y', 'yes', 'Yes', '1', 't', 'T', 'true', 'True']: # a variety of choices to indicate "true"
		bColorMap=True
	else:
		bColorMap=False
else: # if not provided, assume jet colormap
	bColorMap=True


i=Image.open(name)
im=np.array(i)
width, height = im.shape[0], im.shape[1]

bColorBar = True
bDisplay=True

fig,ax = plt.subplots()
if bDisplay:
	implot = imshow(im)
	if bColorBar:
		plt.colorbar(implot,ax=ax)
	plt.axis('off')

	plt.savefig(base_path+'out_with_bar.png')
	plt.show()


if bColorMap:
	cmap = plt.cm.jet
else:
	cmap = plt.cm.binary

minVal = im.min()
maxVal = im.max()
norma = plt.Normalize(vmin=minVal, vmax=maxVal)

if bColorMap:
	im_color = cmap(norma(im)) 
else:
	im_color = cmap(1-norma(im)) # invert for displaying purposes

# save the image
outputName=base_path+'out.png'
pltimg.imsave(outputName, im_color)

